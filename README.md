Docout
======

This library is a functor which creates a `Logging` module, i.e. a `printf` replacement
based on the [`smart-print`](https://github.com/clarus/smart-print) library (its only
dependency).

The module `Docout.Make_logger` takes a `LOGGER_CONFIGURATION` and
produces a `LOGGER` (which includes the `SmartPrint` module as well).

Example
-------

Let's apply the functor to a basic, `stderr` output logger-configuration:

```ocaml
let global_debug_level = ref 2
let global_with_color = ref true
module Log = 
  Docout.Make_logger (struct
    type ('a, 'b) result = 'a
    let debug_level () = !global_debug_level
    let with_color () = !global_with_color
    let line_width = 72
    let indent = 4
    let print_string = Printf.eprintf "%s%!"
    let do_nothing () = ()
    let name = "docout-test"
  end)
```

Then, one can build documents and display them:

```ocaml
Log.(s "Some string " % i 42 % sf ", %S" "quoted"
     % sp % brakets (OCaml.list f [3.14; 4.2])
     @ very_verbose);
```

EDSL Combinators
----------------

On top of all of [`SmartPrint`](https://github.com/clarus/smart-print) values;
`LOGGER` offers:

- `%`: pure concatenation,
- `s`: wrapped string (i.e. Natural language text); the leading and closing
  spaces are preserved,
- `sp`: a breakable space,
- `sf`: like `s` for formats (equivalent to `(s (sprintf "…"))`,
- `i`, `f`: ints; floats,
- `n`: forced new line,
- `verbatim`: non-reformatted strings,
- `exn`: OCaml exceptions,
- `option`: OCaml option combinator,
- `escape`: escape a string (for OCaml lexical conventions),
- ANSI colors: `color`, `bold_red`, `bold_yellow`, `bold_green`, and `greyish`.

The documents created with the EDSL are conditionally passed to
`LOGGER_CONFIGURATION.print_string` with the `@` operator and a *“level”*:
`normal`, `error`, `warning`, `verbose`, or `very_verbose`.

We also provide some *nicer* conversion functions for `SmartPrint.t`:

```ocaml
  val to_string : line_width:int -> indent:int -> SmartPrint.t -> string
  val to_list :
    line_width:int ->
    indent:int ->
    SmartPrint.t ->
    [> `Char of char
    | `String of string
    | `Sub_string of string * int * int ] list
```

