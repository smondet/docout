#!/usr/bin/env ocaml

#use "topfind"
#thread
#require "smart_print"
#directory "_build/"
#load "_build/docout.cma"


let global_debug_level = ref 2
let global_with_color = ref true
module Log = 
  Docout.Make_logger (struct
    type ('a, 'b) result = 'a
    let debug_level () = !global_debug_level
    let with_color () = !global_with_color
    let line_width = 70
    let indent = 4
    let print_string = Printf.eprintf "%s%!"
    let do_nothing () = ()
    let name = "Test_of_docout"
  end)

let print_stuff () =
  Log.(s "some stuff"  @ normal);
  Log.(s "some error"  @ error);
  Log.(s "debug level things" @ verbose);
  Log.(s "debug level things that are much longer than one line bla bla bla bla
          bla bla bla bla bla bla bla bla bla bla bla " @ verbose);
  ()

let () =
  let open Printf in
  print_stuff ();
  global_with_color := false;
  global_debug_level := 0;
  print_stuff ();
  let open Docout in
  let mkd =
    let open Markdown in
    h1 (s "Some Title")
    % s "debug level things that are much longer than one line bla bla bla bla
        bla bla bla bla bla bla bla bla bla bla bla "
    % new_par
    % ul_filter [
        OCaml.list (sf "%S") ["one"; "two"];
        empty;
        s "Bouh";
      ]
    % par (
        s "Some more text with a " 
        % link (s "link" |> emph) ~url:"http://seb.mondet.org"
      )
    % h2 (s "Some Other Title")
  in
  eprintf "Markdown:\n%s\n%!" (Markdown.to_markdown_string mkd)
  

